package io.game_shop.service;

import io.game_shop.model.Book;
import io.game_shop.repository.BookRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BookService {
    @Autowired
    @Qualifier("namedParameterJdbcBookRepository")
    private BookRepository bookRepository;


    public List<Book> getAllBooks() {
        return bookRepository.findAll();
    }

    public int addBook(Book book) {
        System.out.println(book);
        return bookRepository.save(book);
    }

    public int updateBook(Book book, Long id) {
        return bookRepository.update(book,id);
    }

}
