package io.game_shop.service;


import io.game_shop.config.UUIDGenerator;
import io.game_shop.model.Game;
import io.game_shop.repository.GameRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;

@Service
public class GameService {
    @Autowired
    private GameRepository gameRepository;



    public List<Game> getAllGames() {
            List<Game> games = new ArrayList<>();
            gameRepository.findAll().forEach(games::add);
            return games;
    }

    public Game getGame(String gameId) {
      return gameRepository.findByGameId(gameId);
    }

    public Game addGame(Game game) {
        try {
            game.setGameId(UUIDGenerator.generateUniqueKeysWithUUIDAndMessageDigest());

        }
        catch(Exception e) {
            System.out.println("Something went wrong." + e);
        }

        return this.gameRepository.save(game);
    }

    public Game updateGame(Game game, String gameId){
        Game singleGame = gameRepository.findByGameId(gameId);
        singleGame.setGameName(game.getGameName());
        singleGame.setGameDescription(game.getGameDescription());
        return gameRepository.save(singleGame);

    }

}