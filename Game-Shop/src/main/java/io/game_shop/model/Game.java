package io.game_shop.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name="game")
public class Game {
    @Id
    @Column
    private String gameId;
    @Column
    private String gameName;
    @Column
    private String gameDescription;

    public Game() {}

    public Game(String gameId, String gameName, String gameDescription) {
        this.gameId = gameId;
        this.gameName = gameName;
        this.gameDescription = gameDescription;
    }

    public String getGameId() {
        return gameId;
    }

    public void setGameId(String gameId) {
        this.gameId = gameId;
    }

    public String getGameName() {
        return gameName;
    }

    public void setGameName(String gameName) {
        this.gameName = gameName;
    }

    public String getGameDescription() {
        return gameDescription;
    }

    public void setGameDescription(String gameDescription) {
        this.gameDescription = gameDescription;
    }

    @Override
    public String toString() {
        return "Game{" +
                "gameId='" + gameId + '\'' +
                ", gameName='" + gameName + '\'' +
                ", gameDescription='" + gameDescription + '\'' +
                '}';
    }
}
