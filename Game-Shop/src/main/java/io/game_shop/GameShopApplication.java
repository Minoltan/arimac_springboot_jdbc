package io.game_shop;

import org.hibernate.exception.DataException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

import java.util.Date;

@SpringBootApplication
public class GameShopApplication implements CommandLineRunner {

	@Autowired
	JdbcTemplate jdbcTemplate;


	public static void main(String[] args) {
		SpringApplication.run(GameShopApplication.class, args);
	}

	@Override
	public void run(String... args) {
		runJDBC();
	}


	void runJDBC(){
		jdbcTemplate.execute("DROP TABLE IF EXISTS books");
		jdbcTemplate.execute("CREATE TABLE books(" +
				"id SERIAL, name VARCHAR(255), price NUMERIC(15, 2))");
	}

	@Scheduled(fixedRateString = "${someJob.rate}") //PT2H // can use cron = "* * * * * *"
	void someJob() throws InterruptedException {
		System.out.println("Current Time" + new Date());
//		Thread.sleep(1000L);
	}

}



