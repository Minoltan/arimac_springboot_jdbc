package io.game_shop.repository;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

@Repository
public class NamedParameterJdbcBookRepository extends JdbcBookRepository  {

    @Autowired
    private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    /*@Override
    public int update(Book book,Long id) {
        return namedParameterJdbcTemplate.update(
                "update books set price = :price where id = :id",
                new BeanPropertySqlParameterSource(book));
    }
*/

}
