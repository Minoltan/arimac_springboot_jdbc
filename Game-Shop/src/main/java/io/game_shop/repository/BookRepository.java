package io.game_shop.repository;
import io.game_shop.model.Book;

import java.util.List;


public interface BookRepository {

    List<Book> findAll();

    int save(Book book);

    int update(Book book, Long id);

}
