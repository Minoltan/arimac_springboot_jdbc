package io.game_shop.repository;

import io.game_shop.model.Game;
import org.springframework.stereotype.Repository;
import org.springframework.data.repository.CrudRepository;


@Repository
public interface GameRepository extends CrudRepository<Game, String>  {
    Game findByGameId(String gameId);
}
