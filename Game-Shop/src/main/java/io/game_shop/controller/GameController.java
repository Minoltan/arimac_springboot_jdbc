package io.game_shop.controller;


import java.util.List;

import io.game_shop.model.Game;
import io.game_shop.service.GameService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/game")
public class GameController {

    @Autowired
    private GameService gameService;


    @GetMapping("")
    public List<Game> getAllGames(){
        return gameService.getAllGames();
    }

    @GetMapping("{gameId}")
    public Game getGame(@PathVariable("gameId") String gameId){
        return gameService.getGame(gameId);
    }

    @PostMapping()
    public Game addGame(@RequestBody Game game){
       return gameService.addGame(game);
    }

    @PutMapping("{gameId}")
    public Game updateGame(@RequestBody Game game, @PathVariable String gameId){
        return gameService.updateGame(game, gameId);
    }

}
