package io.game_shop.controller;

import io.game_shop.model.Book;
import io.game_shop.service.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


import java.util.List;


@RestController
@RequestMapping("/book")
public class BookController {
    @Autowired
    BookService bookService;

    @GetMapping()
    public List<Book> getAllBooks(){
        return bookService.getAllBooks();
    }

    @PostMapping()
    public int addBook(@RequestBody Book book){
        return bookService.addBook(book);
    }

    @PutMapping("{id}")
    public int updateBook(@RequestBody Book book, @PathVariable Long id){
        return bookService.updateBook(book,id);
    }

}
